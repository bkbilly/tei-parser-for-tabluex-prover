using System;

namespace parser
{
	class ParserNode
	{
		Worker work = new Worker ();

		///<summary> Returns The Binary Tree </summary>
		public BinaryTree GetTree (string thesentence)
		{
			thesentence = work.PutBracketsOnSentence (thesentence);//Put Brackets on every Sentence
			Console.WriteLine (thesentence);
			thesentence = work.FixBrackets (thesentence);//Put Brackets on Sentences based on the operator importance
			Console.WriteLine (thesentence);
			
			BinaryTree mybtree = new BinaryTree (thesentence);
			mybtree = work.Split (thesentence, mybtree);//Get The Tree
			
			return mybtree;
		}

		///<summary> Returns Operator </summary>
		public string GetOperator (string thesentence)
		{
			thesentence = work.PutBracketsOnSentence (thesentence);
			return work.GetSentence (thesentence, 0);
		}

		///<summary> Returns true if it is Negative Atomic Sentence </summary>
		public bool IsNegativeAtomicSentence (string thesentence)
		{
			thesentence = work.PutBracketsOnSentence (thesentence);
			string rightsent = work.GetSentence (thesentence, 1);
			string op = work.GetSentence (thesentence, 0);
			string opsent = work.GetSentence (rightsent, 1);
			if (op == "~" && IsAtomicSentence (opsent) == true) {
				return true;
			} else
				return false;
		}

		///<summary> Returns true if it is Atomic Sentence </summary>
		public bool IsAtomicSentence (string thesentence)
		{
			thesentence = work.PutBracketsOnSentence (thesentence);
			if (work.GetSentence (thesentence, 0) == null)
				return true;
			else
				return false;
		}

		///<summary> Use it to Fix the Sentence's Brackets </summary>
		public string PutBrackets (string thesentence)
		{
			thesentence = work.PutBracketsOnSentence (thesentence);
			return thesentence;
		}

		///<summary> Check if the the sentence is Right </summary>
		public bool IsCorrect (string thesentence)
		{
			//Check for Null
			if (thesentence == "") {
				Console.WriteLine ("You gave NULL");
				return false;
			}

			//Check if Given Parenthesis is Right
			int length = thesentence.Length - 1;
			int bracket = 0;
			for (int i=0; i<=length; i++) {
				if (thesentence [i] == '(')
					bracket++;
				else if (thesentence [i] == ')')
					bracket--;
			}
			if (bracket != 0) {
				Console.WriteLine ("Wrong Parenthesis");
				return false;
			}

			//If anything goes well...
			return true;
		}
	}
}