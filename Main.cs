using System;

namespace parser
{
	class Program
	{
		static void Main (string[] args)
		{
			string value = "";
			do {
				Console.WriteLine ("        _______________________________________");
				Console.WriteLine ("----==============Input the Sentence===============----");
				value = Console.ReadLine ();
				ParserNode input = new ParserNode ();

				if (input.IsCorrect (value) == true)
					input.GetTree (value);

			} while(value!="");
			
			//Console.ReadKey ();
		}
	}
}

//BugTest: P->~(A/\B)
//(A/\B)->C\/R->F