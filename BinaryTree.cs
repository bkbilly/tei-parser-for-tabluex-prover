using System;

namespace parser
{
	public class BinaryTree
	{
		public BinaryTree Left, Right, Parrent;
		public string data;
		
		public BinaryTree (string temp)//constructor create first tree
		{
			data = temp;
		}

		public BinaryTree ()//constructor
		{
			Right = null;
			Left = null;
		}

		public BinaryTree AddRightNode (string data, BinaryTree Parrent)//Right Branch
		{
			BinaryTree newnode = new BinaryTree ();
			newnode.data = data;
			newnode.Parrent = Parrent;
			Right = newnode;
			return newnode;
		}

		public BinaryTree AddLeftNode (string data, BinaryTree Parrent)//Left Branch
		{
			BinaryTree newnode = new BinaryTree ();
			newnode.data = data;
			newnode.Parrent = Parrent;
			Left = newnode;
			return newnode;
		}

		public override string ToString ()//call like: mybtree.right
		{
			return data;
		}
	}
}

