using System;
using System.Collections.Generic;

namespace parser
{
	public class Worker
	{
		///<summary> Returns the Tree </summary>
		public BinaryTree Split (string thesentence, BinaryTree mybtree)
		{
			string op, leftsent, rightsent;
			BinaryTree left, right;
			op = GetSentence (thesentence, 0);
			leftsent = GetSentence (thesentence, 1);
			rightsent = GetSentence (thesentence, 2);

			if (op != null) {
				//Insert to tree
				left = mybtree.AddLeftNode (leftsent, mybtree);
				right = mybtree.AddRightNode (rightsent, mybtree);
				//Recursion
				Split (leftsent, left);
				Split (rightsent, right);

				//Show Results (DEBUGING)
				Console.WriteLine (" --==={0}===--", thesentence);
				Console.WriteLine (leftsent);
				Console.WriteLine (op);
				Console.WriteLine (rightsent);
			}
			return mybtree;
		}

		///<summary> op = 0 || leftsent = 1 ||rightsent = 2 </summary>
		public string GetSentence (string thesentence, int type)
		{
			if (thesentence == "" || thesentence == null)
				return null;
			else if (isChar (thesentence [0]))
				return null;
			bool end = false;
			bool leftfound = false;
			int length = thesentence.Length - 1;
			int openbracket = 0, closebracket = 0;
			int leftbracket = -1;
			int bracket = 0;
			int i = 0;
			bool foundnot = false;
			string box = null;

			while (end == false) {
				if (thesentence [i] == '~')
					foundnot = true;
				else if (isPartPDL (thesentence [i]) == true)
					box += thesentence [i];

				//Find when the brackets close and when leftsent is found
				if (thesentence [i] == '(') {
					bracket++;
					openbracket = i;
					if (type == 1 && leftbracket == -1)
						leftbracket = i;
				} else if (thesentence [i] == ')') {
					bracket--;
					closebracket = i;
					if (bracket == 0)
						leftfound = true;
				}

				//Define when to stop while loop
				if (type == 0 && closebracket < openbracket && leftfound == true)
					end = true;
				else if (type == 1 && leftfound == true) {
					openbracket = leftbracket;
					end = true;
				} else if (type == 2 && leftfound == true && thesentence [i] == '(') {
					openbracket = i;
					closebracket = length;
					end = true;
				}

				
				if (i == length && end == false) {
					if (type == 0 && foundnot == true) //Return Not Operator
						return "~";
					else if (type == 0 && box != null) //Return Box Or Diamond for Operator
						return box;
					else //On Error
						return null;
				}

				
				i++;
			}

			//Check if Operator is Right
			if (type == 0) {
				string op = thesentence.Substring (closebracket + 1, openbracket - closebracket - 1);
				if (!OperatorIsRight (op))
					return "wrong operator";
			}

			//Cut the sentense and return it
			if (type == 0) //Operator
				return thesentence.Substring (closebracket + 1, openbracket - closebracket - 1);
			else if (type == 1) //Left Bracket
				return thesentence.Substring (openbracket + 1, closebracket - openbracket - 1);
			else if (type == 2) //Right Bracket
				return thesentence.Substring (openbracket + 1, closebracket - openbracket - 1);
			else
				return "Wrong Type";
		}

		///<summary> Put Brackets on every sentense </summary>
		public string PutBracketsOnSentence (string thesentence)
		{
			int symbolsfound = 0, i = 0, start = -1, end = -1;
			int bracket = 0;
			int lastposition = 0;
			bool stop = false;
			bool foundbracket = false;

			while (i <= thesentence.Length - 1 && stop == false) {

				//Define when brackets start and close
				if (isChar (thesentence [i]) && start == -1)
					start = i;
				if (isChar (thesentence [i]))
					end = i + 1;

				//Put Start Brackets if ~ or [ or < is found
				if (thesentence [i] == '~' || thesentence [i] == '[' || thesentence [i] == '<') {
					if (i > 0) {
						if (thesentence [i - 1] != '(') {
							thesentence = thesentence.Insert (i, "(");
							symbolsfound++; //how many symbols are found
							lastposition = i;
							i++;
						}
					} else {
						thesentence = thesentence.Insert (i, "(");
						symbolsfound++; //how many symbols are found
						lastposition = i;
						i++;
					}
				}

				//Write the Brackets to thesentence
				if (end != -1 && (!isChar (thesentence [i]) || i == thesentence.Length - 1)) {

					//Stop While if it is the end
					if (i == thesentence.Length - 1) 
						stop = true;

					//Write the Open and Close Brackets If they don't exist
					thesentence = thesentence.Insert (end, ")");
					thesentence = thesentence.Insert (start, "(");
					lastposition += 2;
					i += 2;
					
					start = -1;
					end = -1;
					//Put End Brackets for every ~ or [ or < that is found
					if (symbolsfound > 0) {
						while (lastposition<=thesentence.Length-1) {
							if (thesentence [lastposition] == '(')
								bracket++;
							else if (thesentence [lastposition] == ')') {
								bracket--;
								foundbracket = true;
							}
							if (bracket == 0 && foundbracket == true) {
								foundbracket = false;
								for (int j = 0; j < symbolsfound; j++) {
									thesentence = thesentence.Insert (lastposition, ")");
									i++;
								}
								symbolsfound = 0;
							}
							lastposition++;
						}
						lastposition = 0;
						bracket = 0;
						foundbracket = false;
					}

				}
				i++;
			}
			return thesentence;
		}

		///<summary> Put Brackets to Define Priority of Operator</summary>
		public string FixBrackets (string thesentence)
		{
			int openbracket = 0, closebracket = 0, bracket = 0;
			int i = 0;
			int position;
			bool stop = false;
			string tempop = null;
			bool closechanged = false;

			List<string> OperatorList = new List<string> ();
			List<int> BrCloseList = new List<int> ();
			List<int> BrOpenList = new List<int> ();
			
			//Create lists of every operator and their position
			while (i <= thesentence.Length - 1 && stop == false) {

				//Define the Operator, Position of Open Bracket & Close Bracket
				if (isPartOp (thesentence [i])) {
					tempop += thesentence [i];
					openbracket = i + 1;
					if (closebracket == 0)
						closebracket = i - 1;
				}
				//Fixes a problem with the same symbol for diamond and for ->
				if (tempop != null) {
					if (tempop [0] == '>') {
						tempop = null;
						openbracket = 0;
						closebracket = 0;
					}
				}
				//Put every Operator, Open/Close Bracket into their list
				if (!isPartOp (thesentence [i]) && tempop != null) {
					OperatorList.Add (tempop);
					BrCloseList.Add (closebracket); //Position of the bracket close
					BrOpenList.Add (openbracket);	//Position of the bracket open
					tempop = null;
					openbracket = 0;
					closebracket = 0;
				}
				i++;
			}


			//Put Brackets Left And Right of the Most Important Operator
			while (OperatorList.Count > 1) {
				position = MostImportantOP (OperatorList.ToArray ());
				closechanged = false;

				//Define Position to put Bracket ")" on the Right of the Most Important OP
				i = BrOpenList [position];
				stop = false;
				bracket = 0;
				openbracket = 0;
				closebracket = 0;
				while (i<=thesentence.Length-1 && stop==false) {
					if (thesentence [i] == '(')
						bracket++;
					else if (thesentence [i] == ')')
						bracket--;

					if (bracket == 0) {
						closebracket = i + 1;
						if (i != thesentence.Length - 1) {
							if (thesentence [i + 1] == ')') {
								closechanged = true;
								closebracket++;
							}
						}
						stop = true;
					}
					i++;
				}
				//Define Position to put Bracket "(" on the Left of the Most Important OP
				i = BrCloseList [position];
				stop = false;
				bracket = 0;
				bool brfound = false;
				while (i>=0 && stop==false) {
					if (thesentence [i] == '(') {
						brfound = true;
						bracket++;
					} else if (thesentence [i] == ')')
						bracket--;

					if (bracket == 0 && brfound == true) {
						openbracket = i;
						if (i != 0 && closechanged == true) {
							if (thesentence [i - 1] == '(') {
								openbracket = openbracket - 1;
							} else
								closebracket--;
						}
						stop = true;
					}
					i--;
				}

				//Put Brackets Left and Right of Most Imp Op
				if (NeedsBrackets (thesentence, openbracket, closebracket)) {
					thesentence = thesentence.Insert (closebracket, ")");
					thesentence = thesentence.Insert (openbracket, "(");
					
					//Fix the positions of the brackets after the position
					for (i = position; i < OperatorList.Count; i++) {
						BrOpenList [i] = BrOpenList [i] + 2;
						BrCloseList [i] = BrCloseList [i] + 2;
					}
				}

				//Remove the Used Operators
				OperatorList.RemoveAt (position);
				BrOpenList.RemoveAt (position);
				BrCloseList.RemoveAt (position);
			}


			//Remove the First and Last Bracket if exists
			int length = thesentence.Length - 1;
			stop = false;
			bracket = 0;
			i = 0;
			while (i <= length && stop==false) {
				if (thesentence [i] == '(')
					bracket++;
				else if (thesentence [i] == ')')
					bracket--;
				if (bracket == 0)
					stop = true;
				i++;
			}

			if (i >= length)
				thesentence = thesentence.Substring (1, length - 1);

			return thesentence;
		}

		///<summary> Returns Position of Most Important Operator </summary>
		private int MostImportantOP (string[] OperatorArray)
		{
			int position = 0;
			int i = 0;
			foreach (string str in OperatorArray) {
				if (OpPriority (str) <= OpPriority (OperatorArray [position]))
					position = i;
				i++;
			}
			return position;
		}

		///<summary> Operators Priority </summary>
		private int OpPriority (string op)
		{
			switch (op) {
			case "~":
				return 0;
			case "/\\":
				return 1;
			case "\\/":
				return 2;
			case "->":
				return 3;
			case "|=":
				return 4;
			case "=|":
				return 5;
			case "|-":
				return 6;
			case "-|":
				return 7;
			case "Ξ":
				return 8;
			default:
				return -1;

			}

		}

		/// <summary> Checks if thesentence needs brackets </summary>
		private bool NeedsBrackets (string thesentence, int openbracket, int closebracket)
		{
			//Remove Double Brackets
			int length = thesentence.Length - 1;
			int j;

			for (int i=0; i<length; i++) {
				bool stop = false;
				int bracket = 0;
				j = 0;
				while (stop==false) {//Save Close-Bracket-Position
					if (thesentence [j] == '(')
						bracket++;
					else if (thesentence [j] == ')')
						bracket--;
					if (bracket == 0)
						stop = true;
					j++;
				}
				j--;
				if (openbracket == i && closebracket == j)
					return false;
			}
			return true;

		}

		///<summary> Check if the Operator is Right </summary>
		private bool OperatorIsRight (string op)
		{
			if (op == "/\\" || op == "\\/" || op == "->" || op == "~" || op == "Ξ" ||
				op == "|=" || op == "=|" || op == "|-" || op == "-|")
				return true;
			else
				return false;
		}

		///<summary> Check if it is Not Symbol </summary>
		private bool isChar (char check)
		{
			return (check >= 'A' && check <= 'Z') || (check >= '0' && check <= '9');
		}

		///<summary> Checks if the Char is Part of an Operator </summary>
		private bool isPartOp (char check)
		{
			return (check == '/' || check == '\\' || check == '-' || check == '>' || check == '|' || check == '=' || 
				check == '-' || check == 'Ξ');
		}

		private bool isPartPDL (char check)
		{
			if (check == '<' || check == '>' || check == '[' || check == ']' 
				|| (check >= 'a' && check <= 'z')
				|| check == '?' || check == '*' || check == ';')
				return true;
			else
				return false;
		}
	}
}

/*		0		1		2		3		4		5		6		7		8								*/
/*		Not		And		Or																				*/
/*		~		/\		\/		->		|=		=|		|-		-|		Ξ		[a]		[]		<>		*/
